//
//  MyTableViewCell.h
//  tChat
//
//  Created by Gorbenko Georgy on 23.10.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelMessage;

@property (weak, nonatomic) IBOutlet UIView *viewLabelCell;

@end
