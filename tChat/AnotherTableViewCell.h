//
//  AnotherTableViewCell.h
//  tChat
//
//  Created by Gorbenko Georgy on 23.10.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnotherTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelMessage;

@end
