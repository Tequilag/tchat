//
//  RegisterViewController.m
//  tChat
//
//  Created by Gorbenko Georgy on 24.10.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import "RegisterViewController.h"
@import Firebase;
@import FirebaseAuth;

@interface RegisterViewController ()

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)registerButtonTrapped:(UIButton *)sender {
    
    NSString *email = self.emailTextField.text;
    NSString *pas = self.passwordTextField.text;
    NSString *cpas = self.confirmPasswordTextField.text;
    
    if ([pas isEqualToString:cpas])
    [[FIRAuth auth] createUserWithEmail:email
                               password:pas
                             completion:^(FIRUser *_Nullable user, NSError *_Nullable error) {
                                 if(error){
                                     UIAlertController * alert = [UIAlertController
                                                                  alertControllerWithTitle:nil
                                                                  message:@"Почта введена некорректно или длина пароля меньше 7 символов"
                                                                  preferredStyle:UIAlertControllerStyleAlert];
                                 
                                     UIAlertAction* yesButton = [UIAlertAction
                                                             actionWithTitle:@"ОК"
                                                             style:UIAlertActionStyleDefault
                                                             handler:nil];
                                     [alert addAction:yesButton];
                                     [self presentViewController:alert animated:YES completion:nil];
                                 }
                                     else
                                     [self dismissViewControllerAnimated:YES completion:nil];
                             }];
    else{
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:nil
                                     message:@"Пароли не совпадают"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"ОК"
                                    style:UIAlertActionStyleDefault
                                    handler:nil];
        
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (IBAction)cancelButtonTrapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
