//
//  MyTableViewCell.m
//  tChat
//
//  Created by Gorbenko Georgy on 23.10.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import "MyTableViewCell.h"

@implementation MyTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
@end
