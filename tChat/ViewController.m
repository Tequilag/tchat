//
//  ViewController.m
//  tChat
//
//  Created by Gorbenko Georgy on 19.10.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import "ViewController.h"
@import Firebase;
@import FirebaseAuth;

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   if([[NSUserDefaults standardUserDefaults] boolForKey:@"LoginStatus"])
       [self performSegueWithIdentifier:@"MyView" sender:self];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)enterButton:(id)sender {
    [[FIRAuth auth] signInWithEmail:self.loginTextFiled.text
                           password:self.passwordTextField.text
                         completion:^(FIRUser *user, NSError *error) {
                             if (error){
                                 UIAlertController * alert = [UIAlertController
                                                              alertControllerWithTitle:nil
                                                              message:@"Неверный логин или пароль"
                                                              preferredStyle:UIAlertControllerStyleAlert];
                                 
                                 UIAlertAction* yesButton = [UIAlertAction
                                                             actionWithTitle:@"ОК"
                                                             style:UIAlertActionStyleDefault
                                                             handler:nil];
                                 [alert addAction:yesButton];
                                 [self presentViewController:alert animated:YES completion:nil];
                             }
                             else
                             {
                                 [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"LoginStatus"];
                                 [[NSUserDefaults standardUserDefaults] setObject:user.uid forKey:@"userName"];
                                 self.loginTextFiled.text = nil;
                                 self.passwordTextField.text = nil;
                                 
                                 [self performSegueWithIdentifier:@"MyView" sender:self];
                                 
                             }
                         }];
}

- (IBAction)registerButton:(UIButton *)sender {
    [self performSegueWithIdentifier:@"registerViewModal" sender:self];
}
@end
