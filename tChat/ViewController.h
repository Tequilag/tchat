//
//  ViewController.h
//  tChat
//
//  Created by Gorbenko Georgy on 19.10.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *loginTextFiled;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

- (IBAction)enterButton:(id)sender;

- (IBAction)registerButton:(UIButton *)sender;

@end

