//
//  main.m
//  tChat
//
//  Created by Gorbenko Georgy on 19.10.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
