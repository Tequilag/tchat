//
//  RegisterViewController.h
//  tChat
//
//  Created by Gorbenko Georgy on 24.10.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *registerView;

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;


- (IBAction)registerButtonTrapped:(UIButton *)sender;
- (IBAction)cancelButtonTrapped:(id)sender;
@end
