//
//  ChatViewController.m
//  tChat
//
//  Created by Gorbenko Georgy on 22.10.17.
//  Copyright © 2017 Gorbenko Georgy. All rights reserved.
//

#import "ChatViewController.h"
#import "MyTableViewCell.h"
#import "AnotherTableViewCell.h"

@import Firebase;

@interface ChatViewController () <UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>{
    FIRDatabaseHandle refHandle;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dockViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UITextField *messageInputField;
@property (weak, nonatomic) IBOutlet UITableView *chatTableView;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapGesture;
@property (nonatomic,strong) NSMutableArray *messages;
@property (strong, nonatomic) FIRDatabaseReference *ref;
- (IBAction)exitButtonTapped:(UIBarButtonItem *)sender;

@property (strong, nonatomic) NSString *UDID;

@end

@implementation ChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.messages = [[NSMutableArray alloc] init];
    self.messageInputField.text = nil;
    self.messageInputField.delegate = self;
    self.chatTableView.delegate = self;
    self.chatTableView.dataSource = self;
    self.ref = [[FIRDatabase database] reference];
    
    [self.chatTableView registerNib:[UINib nibWithNibName:NSStringFromClass([MyTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([MyTableViewCell class])];
    [self.chatTableView registerNib:[UINib nibWithNibName:NSStringFromClass([AnotherTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([AnotherTableViewCell class])];
    
    refHandle = [[self.ref child:@"Posts"]  observeEventType:FIRDataEventTypeChildAdded withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        
        NSDictionary *temp = [[NSDictionary alloc] initWithDictionary:snapshot.value];
        
        for (id key in temp){
            NSString *post = [temp valueForKey:key];
            if (post!=nil){
                [self.messages addObject:temp];
                [self.chatTableView reloadData];
                NSIndexPath* ipath = [NSIndexPath indexPathForRow: [self.chatTableView numberOfRowsInSection:0]-1  inSection: 0];
                [self.chatTableView scrollToRowAtIndexPath:ipath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
                
            }
        }
            
    }];
    self.UDID = [[NSUserDefaults standardUserDefaults] objectForKey:@"userName"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)tableViewTap:(UITapGestureRecognizer *)sender{
    [self.messageInputField endEditing:TRUE];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    self.tapGesture.enabled = TRUE;
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:0.5 animations:^{
        self.dockViewHeightConstraint.constant = 310;
        [self.view layoutIfNeeded];
    }completion:nil];
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    self.tapGesture.enabled = FALSE;
    textField.text = nil;
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:0.5 animations:^{
        self.dockViewHeightConstraint.constant = 60;
        [self.view layoutIfNeeded];
    }completion:nil];
}

- (IBAction)sendButtonTapped:(UIButton *)sender {
    if (![self.messageInputField.text isEqualToString:@""]){
        [[[[self.ref child:@"Posts"] childByAutoId] child:self.UDID] setValue:self.messageInputField.text];
        [self.messageInputField endEditing:TRUE];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 140.0;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.messages count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    id cell;
    NSDictionary *temp = [self.messages objectAtIndex:indexPath.row];
    for (id key in temp){
        if ([key  isEqualToString:self.UDID]){
            MyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([MyTableViewCell class]) forIndexPath:indexPath];
            cell.labelMessage.text = [temp valueForKey:key];
            return cell;
        }
        else{
            AnotherTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([AnotherTableViewCell class]) forIndexPath:indexPath];
            cell.labelMessage.text = [temp valueForKey:key];
            return cell;
        }
    }
    return cell;
}

- (IBAction)exitButtonTapped:(UIBarButtonItem *)sender {
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"LoginStatus"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userName"];
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
